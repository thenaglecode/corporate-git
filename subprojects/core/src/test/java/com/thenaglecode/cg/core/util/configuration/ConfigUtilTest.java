package com.thenaglecode.cg.core.util.configuration;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.util.ResourceBundle;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created with IntelliJ IDEA.
 * User: jxnagl
 * Date: 26/02/14
 * Time: 12:52 PM
 * <p/>
 * Tests for the {@link Producers} class
 */
@Slf4j
public class ConfigUtilTest {

    private static final String TEST_RESULT_1 = "abc";
    private static final String TEST_RESULT_2 = "world";
    private static final String TEST_RESULT_3 = "jikes";

    @Test
    public void shouldGiveConfigurationUnderSamePackage() throws NoSuchFieldException, InvocationTargetException, IllegalAccessException {
        ResourceBundle bundle = ConfigUtil.getConfig(this.getClass(), Config.DEFAULT_PACKAGE_NAME,
                Config.DEFAULT_FILE_BASE_NAME, Config.DEFAULT_LANGUAGE, Config.DEFAULT_COUNTRY, Config.DEFAULT_VARIANT);
        assertThat(bundle.getString("test123"), is(TEST_RESULT_1));
    }

    @Test
    public void shouldGiveConfigurationForHelloProperties() throws NoSuchFieldException, InvocationTargetException, IllegalAccessException {
        ResourceBundle bundle = ConfigUtil.getConfig(this.getClass(), Config.DEFAULT_PACKAGE_NAME, "hello", Config.DEFAULT_LANGUAGE,
                Config.DEFAULT_COUNTRY, Config.DEFAULT_VARIANT);
        assertThat(bundle.getString("hello"), is(TEST_RESULT_2));
    }

    /**
     * Property file to obtain:
     *
     */
    @Test
    public void shouldGiveConfigurationInCurrentProjectsPackageRoot() {
        ResourceBundle bundle = ConfigUtil.getConfig(this.getClass(), "core", "myconfiguration");
        assertThat(bundle.getString("test321"), is(TEST_RESULT_3));
    }

    /**
     * This tests the various levels of file resolution for the base name of a resource bundle.
     * packageName =
     * <ul>
     * <li>testproperties</li>
     * <li>core.testproperties</li>
     * <li>cg.core.testproperties</li>
     * <li>com.thenaglecode.cg.core.testproperties</li>
     * </ul>
     */
    @Test
    public void shouldGiveSameConfigurationFile() {
        ResourceBundle bundle1 = ConfigUtil.getConfig(this.getClass(), "testproperties");
        ResourceBundle bundle2 = ConfigUtil.getConfig(this.getClass(), "core.testproperties");
        ResourceBundle bundle3 = ConfigUtil.getConfig(this.getClass(), "cg.core.testproperties");
        ResourceBundle bundle4 = ConfigUtil.getConfig(this.getClass(), "com.thenaglecode.cg.core.testproperties");
        assertThat(bundle1.getString("override1"), is("false"));
        assertThat(bundle2.getString("override1"), is("false"));
        assertThat(bundle3.getString("override1"), is("false"));
        assertThat(bundle4.getString("override1"), is("false"));
    }

    /**
     * This tests the locale configuration hierachy
     */
    @Test
    public void shouldGiveOverridingPropertyFiles() {
        ResourceBundle bundle1 = ConfigUtil.getConfig(this.getClass(), "testproperties", "configuration");
        ResourceBundle bundle2 = ConfigUtil.getConfig(this.getClass(), "testproperties", "configuration", "ja");
        ResourceBundle bundle3 = ConfigUtil.getConfig(this.getClass(), "testproperties", "configuration", "ja", "JP");
        ResourceBundle bundle4 = ConfigUtil.getConfig(this.getClass(), "testproperties", "configuration", "ja", "JP", "jared");
        assertThat(bundle1.getString("override1"), is("false"));
        assertThat(bundle1.getString("override2"), is("false"));
        assertThat(bundle1.getString("override3"), is("false"));
        assertThat(bundle2.getString("override1"), is("true"));
        assertThat(bundle2.getString("override2"), is("false"));
        assertThat(bundle2.getString("override3"), is("false"));
        assertThat(bundle3.getString("override1"), is("true"));
        assertThat(bundle3.getString("override2"), is("true"));
        assertThat(bundle3.getString("override3"), is("false"));
        assertThat(bundle4.getString("override1"), is("true"));
        assertThat(bundle4.getString("override2"), is("true"));
        assertThat(bundle4.getString("override3"), is("true"));
    }
}

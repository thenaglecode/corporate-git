package com.thenaglecode.cg.core.util.misc;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: jxnagl
 * Date: 17/01/14
 * Time: 3:16 PM
 *
 * Test the Thread Utils methods
 */
public class ThreadUtilsTest {

    /**
     * calls a method which in turn calls the Who called me method to determine whether the correct class is returned.
     * @throws ClassNotFoundException
     */
    @Test
    public void shouldEqualSelf() throws ClassNotFoundException {
        assertEquals("This method should return this class", this.getClass(), returnsWhatTheMethodWhoCalledMeReturns());
    }

    /**
     * support method for the shouldEqualSelf test
     * @return the class of the calling method.
     * @throws ClassNotFoundException serious stuff.
     */
    private Class<?> returnsWhatTheMethodWhoCalledMeReturns() throws ClassNotFoundException {
        return ThreadUtils.whoCalledMe();
    }

    /**
     * This util should return the threads count for the
     * @throws ClassNotFoundException
     */
    @Test
    public void shouldEqualSelf2() throws ClassNotFoundException {
        assertEquals("This method should return this class", this.getClass(), ThreadUtilsTestSupport.methodThatCallsNestedMethods());
    }
}

package com.thenaglecode.cg.core.util.misc;

/**
 * Created with IntelliJ IDEA.
 * User: jxnagl
 * Date: 17/01/14
 * Time: 3:53 PM
 * <p/>
 * throw away support class for ThreadUtil tests.
 */
public class ThreadUtilsTestSupport {
    public static Class<?> methodThatCallsNestedMethods() throws ClassNotFoundException {
        return nestedMethod();
    }

    private static Class<?> nestedMethod() throws ClassNotFoundException {
        return ThreadUtils.whoCalledMe(true);
    }
}

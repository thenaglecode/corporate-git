/**
 * Created with IntelliJ IDEA.
 * User: Macindows
 * Date: 1/18/14
 * Time: 9:23 AM
 *
 * Contains classes used for configuration purposes. the idea is that an Instance of
 * {@link au.com.combined.saturn.core.util.configuration.RefreshableBundle}
 * may be annotated with {@link au.com.combined.saturn.core.util.configuration.Config}
 * and using CDI, the bundle may be injected into the field, parameter, setter or constructor.
 */
package com.thenaglecode.cg.core.util;
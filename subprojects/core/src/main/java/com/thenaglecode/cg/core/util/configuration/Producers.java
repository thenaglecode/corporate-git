package com.thenaglecode.cg.core.util.configuration;

import com.thenaglecode.cg.core.util.configuration.cdi.Core;
import lombok.extern.slf4j.Slf4j;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import java.util.ResourceBundle;

/**
 * Created with IntelliJ IDEA.
 * User: jxnagl
 * Date: 14/01/14
 * Time: 1:53 PM
 */
@Slf4j
public class Producers {


    /**
     * this is the main configuration finder for combined.
     *
     * @param ip the point of injection, the RefreshableBundle to replace
     * @return a refreshable bundle according to the configuration in the annotation {@link com.thenaglecode.cg.core.util.configuration.Config}.
     */
    @Produces
    @Core
    private ResourceBundle refreshableBundleCoreProducer(InjectionPoint ip) {
        Config config = ip.getAnnotated().getAnnotation(Config.class);
        if (config == null) {
            return ConfigUtil.getConfig(ip.getBean().getBeanClass(), Config.DEFAULT_PACKAGE_NAME,
                    Config.DEFAULT_FILE_BASE_NAME, Config.DEFAULT_LANGUAGE, Config.DEFAULT_COUNTRY, Config.DEFAULT_VARIANT);
        } else {
            return getConfig(ip.getBean().getBeanClass(), config);
        }
    }


    private static ResourceBundle getConfig(Class<?> callingClass, Config config) {
        //package names allowed:
                /*
        * au.com.combined.saturn.core...
        * saturn.core...
        * core...
        * ...
        * */
        return ConfigUtil.getConfig(callingClass, config.packageName(), config.fileBaseName(),
                config.language(), config.country(), config.variant());
    }


}

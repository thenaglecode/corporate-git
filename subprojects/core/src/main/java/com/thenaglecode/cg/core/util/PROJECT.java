package com.thenaglecode.cg.core.util;

import lombok.extern.slf4j.Slf4j;
import com.thenaglecode.cg.core.util.misc.ThreadUtils;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: jxnagl
 * Date: 27/02/14
 * Time: 4:27 PM
 * <p/>
 * Convenience class to retrieve meta information about the current project.
 * <p/>
 * Each project has a generated project.properties file under the root of the resource directory in the compiler output.
 * This allows the builder to inject properties into the application during build time. basic properties may include
 * the project's name, projects this project depends on, etc...
 */
@Slf4j
public class PROJECT {

    public static final String PROJECT_PROPERTIES_FILE = "project.properties";
    public static final String PROJECT_NAME_KEY = "name";
    public static final Map<String, Properties> ALL_PROJECTS = new HashMap<>();
    private static final Map<Class<?>, Properties> CLASS_PROPERTY_CACHE = new HashMap<>();

    public static Properties properties() {
        try {
            return properties(ThreadUtils.whoCalledMe(true));
        } catch (ClassNotFoundException e) {
            log.error("He's dead Jim", e);
            return null;
        }
    }

    /**
     * Project dependant getter to retrieve the project.properties file located at {@value #PROJECT_PROPERTIES_FILE}
     *
     * @return the project properties.
     */
    public static Properties properties(Class<?> clazz) {
        Properties properties = CLASS_PROPERTY_CACHE.get(clazz);
        if(properties != null) {
            return properties;
        }
        try {
            properties = new Properties();
            URI uri = clazz.getResource("/" + PROJECT_PROPERTIES_FILE).toURI();
            log.debug("project properties file located at - " + uri.toString());
            Path propertyFilePath = Paths.get(uri);
            if (!Files.exists(propertyFilePath)) {
                throw new RuntimeException("The project property file (" + PROJECT_PROPERTIES_FILE
                        + ") is missing! calling class: " + clazz.getName());
            }
            properties.load(Files.newInputStream(propertyFilePath));
            ALL_PROJECTS.put(properties.getProperty(PROJECT_NAME_KEY), properties);
            CLASS_PROPERTY_CACHE.put(clazz, properties);
            return properties;
        } catch (URISyntaxException e) {
            log.error("uri syntax error: ", e);
        } catch (IOException e) {
            log.error("Could not read the properties file", e);
        }
        return null;
    }

    /**
     * Convenience method that returns the current project name according to the person who called this method.
     *
     * @return the current project name.
     */
    public static String getProjectName() {
        return properties().getProperty(PROJECT_NAME_KEY);
    }

    /**
     * Convenience method that returns the project name of the calling class.
     *
     * @param clazz the class whos holding project will be determined.
     * @return the given class's project name.
     */
    public static String getProjectName(Class<?> clazz) {
        return properties(clazz).getProperty(PROJECT_NAME_KEY);
    }
}

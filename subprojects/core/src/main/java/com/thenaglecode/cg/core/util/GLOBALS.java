package com.thenaglecode.cg.core.util;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

/**
 * Created with IntelliJ IDEA.
 * User: jxnagl
 * Date: 26/02/14
 * Time: 11:09 AM
 *
 * Contains Critical Global variables loaded at application startup.
 *
 */
public class GLOBALS {
    public static final String CORE_CONFIG_RESOURCE_NAME = "com.thenaglecode.cg.core.configuration";

    private static final String DOMAIN_PACKAGE_NAME_KEY = "app.domain.package.name";
    /** The domain fileBaseName prefix for all the app packages */
    public static final String DOMAIN_PACKAGE_NAME;

    private static final String APP_NAME_KEY = "app.name";
    /** The Application's fileBaseName */
    public static final String APP_NAME;

    private static final String APP_PACKAGE_NAME_KEY = "app.package.name";
    /** The fileBaseName contained within package names for this app */
    public static final String APP_PACKAGE_NAME;


    static {
        ResourceBundle bundle = PropertyResourceBundle.getBundle(CORE_CONFIG_RESOURCE_NAME);
        DOMAIN_PACKAGE_NAME = bundle.getString(DOMAIN_PACKAGE_NAME_KEY);
        APP_NAME = bundle.getString(APP_NAME_KEY);
        APP_PACKAGE_NAME = bundle.getString(APP_PACKAGE_NAME_KEY);
    }
}

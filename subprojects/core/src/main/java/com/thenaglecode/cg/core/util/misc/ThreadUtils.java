package com.thenaglecode.cg.core.util.misc;

/**
 * Created with IntelliJ IDEA.
 * User: jxnagl
 * Date: 17/01/14
 * Time: 3:06 PM
 *
 * Utility for thread related activities.
 *
 * <ul>
 *     <li>whoCalledMe - Allows the caller to find which class called this caller's function.</li>
 * </ul>
 */
public class ThreadUtils {

    /**
     * returns the class object of the method that called the caller of this method
     * @return the class object of the method that called the caller of this method.
     * @throws ClassNotFoundException if the class could not be obtained from the current classloader.
     */
    public static Class<?> whoCalledMe() throws ClassNotFoundException {
        return whoCalledMe(false);
    }

    /**
     * returns either the calling class or if <code>excludingThisClass</code> is true, the last class
     * that isn't this class that called this class.
     * @param excludingThisClass if true it will go up the stack trace until the class is not the caller.
     * @return the class that called
     * @throws ClassNotFoundException
     */
    public static Class<?> whoCalledMe(boolean excludingThisClass) throws ClassNotFoundException {
        boolean visitedThisThread = false;
        boolean visitedCallingThread = false;
        String classNameOfCaller = null;
        for(StackTraceElement element : Thread.currentThread().getStackTrace()){
            String className = element.getClassName();
            if(visitedThisThread && visitedCallingThread){
                if(!excludingThisClass)
                    return Class.forName(className);
                else if(!className.equals(classNameOfCaller)){
                    return Class.forName(className);
                }
            } else if(ThreadUtils.class.getName().equals(className)){
                visitedThisThread = true;
            } else if (visitedThisThread){
                visitedCallingThread = true;
                classNameOfCaller = element.getClassName();
            }
        }
        throw new RuntimeException("Could not determine calling class. What sorcery is this?");
    }
}

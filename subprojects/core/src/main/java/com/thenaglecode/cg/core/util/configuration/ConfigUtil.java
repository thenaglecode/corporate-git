package com.thenaglecode.cg.core.util.configuration;

import com.thenaglecode.cg.core.util.GLOBALS;
import com.thenaglecode.cg.core.util.PROJECT;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Created by IntelliJ IDEA.
 * User: jarednagle
 * Date: 5/03/2014
 * Time: 7:50 AM
 */
public class ConfigUtil {
    public static final String PREFIX_1 = GLOBALS.DOMAIN_PACKAGE_NAME + "." + GLOBALS.APP_PACKAGE_NAME + ".";
    public static final String PREFIX_2 = GLOBALS.APP_PACKAGE_NAME + ".";

    /**
     * This method resolves a package name into a valid package base off these rules:
     * <ul>
     * <li>
     * If the package starts with {@link #PREFIX_1} or {@link #PREFIX_2}
     * Then check whether the next name is a valid project.
     * </li>
     * <li></li>
     * <li></li>
     * <li></li>
     * </ul>
     *
     * @param callingClass
     * @param packageName
     * @return
     */
    private static String resolveFullPackageName(Class<?> callingClass, String packageName) {
        if (packageName.isEmpty()) {
            String projectName = PROJECT.getProjectName(callingClass);
            return PREFIX_1 + projectName.toLowerCase()
                    + getSubsystemSuffix(callingClass, packageName, projectName);
        } else {
            String specifiedName = extractProjectOrRelativeName(callingClass, packageName);
            int index = packageName.indexOf(specifiedName);
            String suffix = "";
            if (index > -1 && packageName.length() != specifiedName.length()) {
                suffix = packageName.substring(packageName.indexOf(specifiedName) + specifiedName.length());
            }
            if (PROJECT.ALL_PROJECTS.containsKey(specifiedName)) {
                // we can now build a valid package name from the given information.
                return PREFIX_1 + specifiedName + suffix;
            } else {
                // assume that we want a sub directory of the current project
                return PREFIX_1 + PROJECT.getProjectName(callingClass) + "." + specifiedName + suffix;
            }
        }
    }

    /**
     * Called During packageResolution to determine the project this package name refers to.
     *
     * @param packageName the packageName with a prefix
     * @return the project name that is specified by this package.
     */
    private static String extractProjectOrRelativeName(Class<?> callingClass, String packageName) {
        if (packageName.startsWith(PREFIX_1)) {
            String sub = packageName.substring(PREFIX_1.length());
            return sub.substring(0, sub.indexOf("."));
        } else if (packageName.startsWith(PREFIX_2)) {
            String sub = packageName.substring(PREFIX_2.length());
            return sub.substring(0, sub.indexOf("."));
        } else {
            int index = packageName.indexOf(".");
            if (index < 0) {
                return packageName; //this is the project name
            } else {
                return packageName.substring(0, index);
            }
        }
    }

    private static String getSubsystemSuffix(Class<?> callingClass, String packageName, String projectName) {
        if (packageName.isEmpty()) {
            // we want the current directory
            String currentPackage = callingClass.getPackage().getName();
            int index = currentPackage.indexOf(projectName);
            if (index + projectName.length() + 1 == currentPackage.length()) {
                return "";
            } else {
                return currentPackage.substring(index + projectName.length());
            }
        } else {
            int index = packageName.indexOf(projectName);
            if(index + projectName.length() == packageName.length()){
                return "";
            } else {
                return packageName.substring(index + projectName.length());
            }
        }
    }

    /**
     * Get Configuration by specifying a variable number of arguments according to which locale you need.
     *
     * @param args in order: the package name, the baseFileName, language locale, country locale and variant locale.
     * @return the resource bundle found via the given arguments.
     */
    public static ResourceBundle getConfig(Class<?> callingClass, String... args) {
        if (args.length < 1 || args.length > 5) {
            throw new IllegalArgumentException("expected 1 - 5 variants! got: " + args.length);
        }
        Locale.Builder builder = new Locale.Builder();
        String fileBaseName = "configuration";
        try {
            fileBaseName = args[1];
            builder.setLanguage((args[2].isEmpty()) ? Locale.getDefault().getLanguage() : args[2]);
            builder.setRegion((args[3].isEmpty()) ? Locale.getDefault().getCountry() : args[3]);
            builder.setVariant((args[4].isEmpty()) ? Locale.getDefault().getVariant() : args[4]);
        } catch (Exception ignored) {
            // continue
        }
        String fullBaseName = resolveFullPackageName(callingClass, args[0]);
        fullBaseName = (fullBaseName.isEmpty()) ? fileBaseName : fullBaseName + "." + fileBaseName;
        return ResourceBundle.getBundle(fullBaseName, builder.build(), callingClass.getClassLoader());
    }
}

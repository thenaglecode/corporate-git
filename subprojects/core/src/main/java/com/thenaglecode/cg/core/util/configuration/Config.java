package com.thenaglecode.cg.core.util.configuration;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created with IntelliJ IDEA.
 * User: jxnagl
 * Date: 14/01/14
 * Time: 1:45 PM
 * <p/>
 * Configuration properties of an injectable ResourceBundle.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(value = {ElementType.FIELD, ElementType.CONSTRUCTOR, ElementType.METHOD, ElementType.PARAMETER})
public @interface Config {

    public static final String DEFAULT_PACKAGE_NAME = "";
    public static final String DEFAULT_FILE_BASE_NAME = "configuration";
    public static final String DEFAULT_LANGUAGE = "";
    public static final String DEFAULT_COUNTRY = "";
    public static final String DEFAULT_VARIANT = "";

    /**
     * Specify the Package fileBaseName relative to GLOBALS.PACKAGE_NAME;
     *
     * @return the package fileBaseName relative to the And Subsystem
     */
    String packageName() default "";

    /**
     * the base fileBaseName of the configuration file without all the language/country additions.
     * (e.g. if you have a file named configuration_en_PT.properties, it's base
     * fileBaseName is "configuration")<br/>
     * default fileBaseName is "configuration"
     *
     * @return the base fileBaseName of the configuration file.
     */
    String fileBaseName() default "configuration";

    /**
     * Locale of the configuration document. defaults to @{link Locale#getDefault()}
     * @return the language of the configuration document.
     */
    String language() default "";

    /**
     * The country of the configuration file. defaults to Locale.()
     * @return
     */
    String country() default "";

    String variant() default "";
}
